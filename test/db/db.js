
/* Links
  https://www.npmjs.com/package/sqlite3
  http://www.sqlitetutorial.net/sqlite-nodejs/connect/
  https://github.com/mapbox/node-sqlite3/wiki/API
*/

async function log (dbpath, commit_hash, timestamp, test_file, JSON_result) {

    var sqlite3 = require('sqlite3').verbose();

    const test_table = commit_hash + "_" + timestamp;

    //TODO
    const comment = 'Hello world!';
    const cmd = 'Blah -octopus';

    //https://www.npmjs.com/package/jsonpack
    var jsonpack = new require("jsonpack/main");
    var JSON_result_packed = jsonpack.pack(JSON_result);

    let db = new sqlite3.Database(dbpath, sqlite3.OPEN_CREATE | sqlite3.OPEN_READWRITE , (err) => {
        if (err) {
            console.error(err.message);
        }
    });

  // These could be optimize, but who really care or want to do more SQL
  db.serialize(function() {
    db.run("CREATE TABLE IF NOT EXISTS git_commit (commit_hash TEXT PRIMARY KEY)");
    db.run("CREATE TABLE IF NOT EXISTS test (commit_hash TEXT, timestamp TEXT, comment TEXT,\
            command_line TEXT, PRIMARY KEY (commit_hash, timestamp))");
    db.run("CREATE TABLE IF NOT EXISTS " + test_table +
           "(test_file TEXT PRIMARY KEY, JSON_result_packed TEXT)");

    db.run("INSERT INTO git_commit (commit_hash) VALUES ('" + commit_hash + "')", (err) => {});
    db.run("INSERT INTO test (commit_hash, timestamp, comment, command_line) VALUES\
           (('" + commit_hash  +  "'), ('" + timestamp  + "'), ('" + comment  + "'),\
           ('" + cmd  + "'))", (err) => {});
    db.run("INSERT INTO " + test_table + " (test_file, JSON_result_packed) VALUES\
           (('" + test_file + "'), ('" + JSON_result_packed  + "'))", (err) => {});
  });

    db.close();
}

try {
  module.exports.log = log;
} catch (e) {
  // Ignore these, as we're not using node
}
