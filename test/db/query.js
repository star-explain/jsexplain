#!/usr/bin/env node

// https://www.freecodecamp.org/news/writing-command-line-applications-in-nodejs-2cf8327eee2/
// https://stackoverflow.com/questions/41319756/function-to-return-data-from-sqlite3-query-in-node-js

'use strict';

var jsexplain_db = __dirname + '/jsexplain_test.db'

/*
  Store all query result in an array then convert then using the format callback
*/
 function query_db(dbpath, query, format) {

  var sqlite3 = require('sqlite3').verbose();  

  let db = new sqlite3.Database(dbpath, sqlite3.OPEN_CREATE |
                                sqlite3.OPEN_READWRITE , (err) => {
        if (err) {
            console.error(err.message);
        }
    });

  return new Promise (
   function (resolve, reject) {
    db.serialize(function() {
     var ret = [];
     db.each( query,
              (err, row) => { ret.push(format(row)); },
              function () { db.close(); resolve(ret); } );
    })
   }
  );

}

/*
  Return all rows from test table of a specific commit
*/
function get_commit_test_runs (commit_hash) {

  var sql =  "SELECT rowid, commit_hash, timestamp, comment, command_line\
              FROM test\
              WHERE commit_hash = '" + commit_hash + "'";

  return query_db(jsexplain_db, sql, function (rows) { return rows;});
}


function get_commented_tests (com1, com2) {
  var sql =  "SELECT rowid, commit_hash, timestamp, comment, command_line\
              FROM test\
              WHERE comment = '" + com1 + "' OR comment = '" + com2 + "'" ;

  return query_db(jsexplain_db, sql, function (rows) { return rows;});
}

/***************************** TESTS ******************************************/

/*
  Return all tables names for a commit
*/
function test_get_commit_test_runs() {
  var promise = get_commit_test_runs('fc6c6b5e61560ff89eff834b05e89e6fd4caa79f');
  promise.then(
    function(result) {
      console.log(
        result.map( (r) => r.commit_hash + '_' + r.timestamp)
      );
    }
  );
}

var comment0 = 'coucou';
var ts0 = '1561446355758';
var comment1 = 'Wtf666';

/*
  Return tests table of two commented test
*/
function test_get_test_tables_from_comment (com1, com2) {
  var promise =  get_commented_tests (com1, com2) ;
  return promise.then(
    function(result) {
         result.map ( (r) => ({comment : r.comment, table:  r.commit_hash + '_' + r.timestamp}) );
    }
  )
}

//test_get_commit_test_runs();
console.log (test_get_test_tables_from_comment (comment0, comment1));

