import sys
import re # regular expression

#
# This script remove the (long) log of recursive call failure in test262 tests suite
#


# Process f on lines
def processing(lines, f):
    ret = None
    for i in range(len(lines)):
        ret = f(i, lines[i], ret)
    return ret

def find_range_err_and_tests(i, l, ret):
    s = l.strip()
    if 'JSEXPLAIN_DIR/test/data/test262' in s:
        return [(i, 'Test')] if ret == None else ret + [(i, 'Test')]
    elif s == 'RangeError: Maximum call stack size exceeded':
        return [(i, 'RangeError')] if ret == None else ret + [(i, 'RangeError')]
    return ret

if len(sys.argv) > 1:

    arg1 = sys.argv[1]
    
    try:
        f = open(arg1, 'r')
        lines = f.readlines()
        f.close()

        tests_and_range_err = processing(lines, find_range_err_and_tests)

        def is_kind_tuple(e, k):
            _, v = e
            return v == k

        tests = list(filter(lambda e : is_kind_tuple(e, 'Test') ,tests_and_range_err))
        range_err = list(filter(lambda e : is_kind_tuple(e, 'RangeError') ,tests_and_range_err))

        # Line number only of each test
        def proj1 (pair):
            i, _ = pair
            return i

        tests_line = list(map(proj1, tests))

        # Line number only of each range_err
        range_err_lines = list(map(proj1, range_err))

        # Log line interval of each test
        tests_line_interval = []

        for i in range(len(tests_line)-1):
            tests_line_interval += [(tests_line[i], tests_line[i+1]-1)]
        
        tests_line_interval += [(tests_line[len(tests_line)-1], len(lines)-1)] # Last test

        # Find line interval to delete
        line_interval_to_delete = []
        
        for i in range(len(tests_line_interval)):
            minn, maxx = tests_line_interval[i]
            for j in range(len(range_err_lines)):
                line_rgerr = range_err_lines[j]
                if minn <= line_rgerr and line_rgerr <= maxx:
                    line_interval_to_delete += [tests_line_interval[i]]

        # Write cleaned file version
        f = open(arg1 + '_cleaned.log', 'a')

        # Test if a line number is in an interval in a liste of interval
        def num_in_list_of_interval(n, lint):
            for i in range(len(lint)):
                minn, maxx = lint[i]
                if minn <= n and n <= maxx:
                    return True
            return False

        for i in range(len(lines)):
            if not num_in_list_of_interval(i, line_interval_to_delete):
                f.write(lines[i])
            
        f.close()
        #print(line_interval_to_delete)

    except IOError:
        print('File ' + f +  ' does not exist')
else :
    print('Provide an argument')
