#!/bin/bash

SCRIPT_CURR_DIR="$(pwd)"

# Global Variables
JSEXPLAIN_GIT_REPO_URL="https://gitlab.inria.fr/star-explain/jsexplain"
FJS_OF_FML_GIT_REPO_URL="https://gitlab.inria.fr/star-explain/fjs_of_fml"
COMPARE_COMMIT_DIR="/tmp/compare_commit"
FJS_OF_FML_DIR=$COMPARE_COMMIT_DIR/fjs_of_fml
FJS_DIR1=$FJS_OF_FML_DIR/fjs1
FJS_DIR2=$FJS_OF_FML_DIR/fjs2
JSEXPLAIN_DIR=$COMPARE_COMMIT_DIR/jsexplain
JSEXP_DIR1=$JSEXPLAIN_DIR/jsexp1
JSEXP_DIR2=$JSEXPLAIN_DIR/jsexp2
FJS_BRANCH1=evol
FJS_BRANCH2=evol
JSEXP_BRANCH1=evol_driver
JSEXP_BRANCH2=evol_driver
FJS_HASH1=
FJS_HASH2=
JSEXP_HASH1=fc6c6b5e
JSEXP_HASH2=
FJS_NATIVE1=$FJS_DIR1/fjs_of_fml.native
FJS_NATIVE2=$FJS_DIR2/fjs_of_fml.native
FJS_BYTE1=$FJS_DIR1/fjs_of_fml.byte
FJS_BYTE2=$FJS_DIR1/fjs_of_fml.byte
FJS_BIN1=$FJS_NATIVE1
FJS_BIN2=$FJS_NATIVE1
PYTHON=python3.5
PY_SCRIPT_CLEAN_RANGE_ERR=${SCRIPT_CURR_DIR}/filter_rangerror.py

# This the test from test262 you want to run
TEST_STR=

# Reset a git repository DIR to commit HASH
# If HASH is empty nothing is done (we stay on head)
repo_to_commit_hash()
{
 DIR=$1
 HASH=$2

 if [ ! -z "$HASH" ]; then
   CURRENT_DIR="$(pwd)"
   cd "$DIR"
   git reset --hard "$HASH"
   cd "$CURRENT_DIR"
 fi
}

# Clone a git repository from URL to DIR.
# Delete DIR if exist
clone_git_repo()
{
 URL=$1
 DIR=$2

 if [ -d "$DIR" ]; then
   rm -r -f "$DIR"
 fi
 mkdir $DIR
 git clone $URL $DIR
}

# Clone repo URL in DIR/HASH1 and DIR/HASH2
# Delete DIR if exist
set_compare ()
{
  DIR=$1
  URL=$2
  HASH1=$3
  HASH2=$4

  if [ -d "$DIR" ]; then
    rm -r -f "$DIR"
  fi
  mkdir $DIR

  DIR1="$DIR/$HASH1"
  mkdir $DIR1

  DIR2="$DIR/$HASH2"
  mkdir $DIR2

  clone_git_repo $URL $DIR1
  clone_git_repo $URL $DIR2
}

# Checkout BRANCH in DIR repo
# If there is no branch (we stay on master) nothing is done
branch ()
{
  DIR=$1
  BRANCH=$2

  if [ ! -z "$BRANCH" ]; then
    CURRENT_DIR="$(pwd)"
    echo $DIR :
    cd "$DIR"
    git checkout "$BRANCH"
    cd "$CURRENT_DIR"
  fi
}

# Create all folders
# Checkout git repo in folders
# Checkout branch for each repo
main_env()
{
  # Delete COMPARE_COMMIT_DIR if exist
  if [ -d "$COMPARE_COMMIT_DIR" ]; then
    rm -r -f "$COMPARE_COMMIT_DIR"
  fi
  
  # Create all folders
  mkdir $COMPARE_COMMIT_DIR
  mkdir $FJS_OF_FML_DIR
  mkdir $JSEXPLAIN_DIR
  mkdir $FJS_DIR1
  mkdir $FJS_DIR2
  mkdir $JSEXP_DIR1
  mkdir $JSEXP_DIR2

  # Checkout git repo in folders
  clone_git_repo $JSEXPLAIN_GIT_REPO_URL $JSEXP_DIR1
  clone_git_repo $JSEXPLAIN_GIT_REPO_URL $JSEXP_DIR2
  clone_git_repo $FJS_OF_FML_GIT_REPO_URL $FJS_DIR1
  clone_git_repo $FJS_OF_FML_GIT_REPO_URL $FJS_DIR2

  # Checkout branch for each repo
  branch $JSEXP_DIR1 $JSEXP_BRANCH1
  branch $JSEXP_DIR2 $JSEXP_BRANCH2
  branch $FJS_DIR1 $FJS_BRANCH1
  branch $FJS_DIR2 $FJS_BRANCH2
}

# Show current commit hash and message of DIR
current_commit()
{
  DIR=$1
  CURRENT_DIR="$(pwd)"
  cd $DIR
  BRANCH=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')
  HASH=$(git rev-parse HEAD)
  MSG=$(git log --format=%B -n 1 $HASH)
  cd $CURRENT_DIR
  echo "$DIR :
  Branch  : $BRANCH
  Commit  : $HASH
  Message : $MSG"
}

# Set all repo to the right commit
set_repo_to_commit_hashs()
{
  repo_to_commit_hash $JSEXP_DIR1 $JSEXP_HASH1
  #current_commit $JSEXP_DIR1
  repo_to_commit_hash $JSEXP_DIR2 $JSEXP_HASH2
  #current_commit $JSEXP_DIR2
  repo_to_commit_hash $FJS_DIR1 $FJS_HASH1
  #current_commit $FJS_DIR1
  repo_to_commit_hash $FJS_DIR2 $FJS_HASH2
  #current_commit $FJS_DIR2
}

# Build fjs_of_fml in DIR
build_dir_fjs()
{
  DIR=$1
  CURRENT_DIR="$(pwd)"
  cd $DIR
  echo $DIR
  autoconf
  ./configure
  make 
  cd $CURRENT_DIR
}

# Replace 2 lines in JSExplain Makefile.in to build it with the extracted fjs_of_fml (in FJS_DIR2 dir)
fix_jsexp_Makefile()
{
MAKEFILE=$1

# Working command to replace "cd $(CURRENT_DIR)/jsref ; autoconf ; ./configure" string in a sedfox sample file :
#sed -i "s/cd \$(CURRENT_DIR)\/jsref ; autoconf ; .\/configure/cd \$(CURRENT_DIR)\/jsref ; autoconf ; .\/configure GENERATOR_DIR='\$(GENERATOR_DIR)'/g" sedfix
MAKEFILE_ORI_CMD1="cd \$(CURRENT_DIR)\/jsref ; autoconf ; .\/configure"
MAKEFILE_NEW_CMD1="cd \$(CURRENT_DIR)\/jsref ; autoconf ; .\/configure GENERATOR_DIR='\$(GENERATOR_DIR)'"

MAKEFILE_ORI_CMD2="\$(MAKE) -C jsref jsjsref; \$(MAKE) -C jsref mljsref"
MAKEFILE_NEW_CMD2="\$(MAKE) -C jsref jsjsref STDLIB_DIR='\$(GENERATOR_DIR)\/src\/stdlib_fml'; \$(MAKE) -C jsref mljsref STDLIB_DIR='\$(GENERATOR_DIR)\/src\/stdlib_fml'"

sed -i "s/$MAKEFILE_ORI_CMD1/$MAKEFILE_NEW_CMD1/g" "$MAKEFILE"
sed -i "s/$MAKEFILE_ORI_CMD2/$MAKEFILE_NEW_CMD2/g" "$MAKEFILE"
}

# Build JSExplain with the updated Makefile
build_dir_jsexp()
{
  DIR=$1
  GENDIR=$2
  CURRENT_DIR="$(pwd)"
  cd $DIR
  echo $DIR
  MAKEFILE=$DIR/Makefile.in 
  # Fix Makefile
  fix_jsexp_Makefile $MAKEFILE
  autoconf
  ./configure
  make GENERATOR_DIR=$GENDIR  STDLIB_DIR=$GENDIR/src/stdlib_fml
  cd $CURRENT_DIR
}

# Show all repository status
show_all_current_commit()
{
  current_commit $JSEXP_DIR1
  current_commit $JSEXP_DIR2
  current_commit $FJS_DIR1
  current_commit $FJS_DIR2
}

# Configure repository for test262 in jsexp directory DIR
jsexp_test_init()
{
  DIR=$1
  make -C $DIR test_init
}

# Install and configure npm in jsexp directory
jsexp_conf_npm()
{
  DIR=$1
  make -C $DIR npm
}

# Run the test in jsexp dir, a log file is generate in it
test_jsexp()
{
  DIR=$1
  STR_GREP=$2
  CURRENT_DIR="$(pwd)"
  cd $DIR
  BASE_NAME="$(basename $DIR)"
  if [ -z "$STR_GREP" ]; then
      node_modules/.bin/mocha | tee test_$BASE_NAME.log
  else
      node_modules/.bin/mocha --grep $STR_GREP | tee test_$BASE_NAME.log
  fi
  cd $CURRENT_DIR
}

normalize_log()
{
  DIR=$1
  LOG_FILE=$DIR/test_$(basename $DIR).log
  # backup ori log
  cp $LOG_FILE $LOG_FILE.ori
  DIR_ESCAPED="${DIR//'/'/"\/"}"
  # normalize path
  sed -i "s/$DIR_ESCAPED/JSEXPLAIN_DIR/g" "$LOG_FILE"

  # Time
  sed -i "s/([0-9]*ms)//g" "$LOG_FILE"
  sed -i "s/([0-9]*m)//g" "$LOG_FILE"

  # interpreter line
  sed -i "s|(test/interpreter.js:[0-9]*:[0-9]*)|test/interpreter.js::)|g" "$LOG_FILE"

  # colors
  sed -i 's/[[0-9]*m//g' "$LOG_FILE"

  # error number
  #sed -i 's/      [0-9]*)/#)/g' "$LOG_FILE"
}

npm_install_module()
{
  DIR=$1
  CURRENT_DIR="$(pwd)"
  MODULE=$2
  npm install $MODULE
  cd $CURRENT_DIR
}

range_error_cleaning()
{
  FILE=$1
  PY_SCRIPT=$2
  PYT_RUNTIME=$3

  $PYT_RUNTIME $PY_SCRIPT $FILE
}

main()
{
  main_env
  set_repo_to_commit_hashs

  build_dir_fjs $FJS_DIR1
  build_dir_jsexp $JSEXP_DIR1 $FJS_DIR1

  build_dir_fjs $FJS_DIR2
  build_dir_jsexp $JSEXP_DIR2 $FJS_DIR2

  jsexp_test_init $JSEXP_DIR1
  jsexp_test_init $JSEXP_DIR2

  jsexp_conf_npm $JSEXP_DIR1
  jsexp_conf_npm $JSEXP_DIR2

  test_jsexp $JSEXP_DIR1 $TEST_STR
  test_jsexp $JSEXP_DIR2 $TEST_STR

  normalize_log $JSEXP_DIR1
  normalize_log $JSEXP_DIR2

  show_all_current_commit
}

main

LOG_FILE1=$JSEXP_DIR1/test_$(basename $JSEXP_DIR1).log
LOG_FILE2=$JSEXP_DIR2/test_$(basename $JSEXP_DIR2).log

range_error_cleaning $LOG_FILE1 $PY_SCRIPT_CLEAN_RANGE_ERR $PYTHON
range_error_cleaning $LOG_FILE2 $PY_SCRIPT_CLEAN_RANGE_ERR $PYTHON

CLEAN_LOG_FILE1=${LOG_FILE1}_cleaned.log
CLEAN_LOG_FILE2=${LOG_FILE2}_cleaned.log

diff -w $CLEAN_LOG_FILE1 $CLEAN_LOG_FILE2 > $COMPARE_COMMIT_DIR/diff_cleaned.log

cat $COMPARE_COMMIT_DIR/diff_cleaned.log
