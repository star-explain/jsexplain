
# Package OPAM jsexplain
At this point the jsexplain package is not released in the official OPAM repository,

## Installation and test with OPAM
```
opam switch create jsexplain 4.07.1
eval $(opam env)


opam pin -yn add JS_Parser "https://github.com/resource-reasoning/JS_Parser.git#v0.1.0"
opam install -y JS_Parser


opam repository add jsexplain https://gitlab.inria.fr/star-explain/opam-repository.git#add-pkg-fjs_of_fml


opam install -y jsexplain


firefox ~/.opam/jsexplain/share/jsexplain/driver.html&
```

## Maintenance
WARNING : The _opam-repository_ should be in the parent folder of this one (_../opam-repository_).
Here is the worklow to update the tarball:
1. Commit and push your change.
2. Run `make git_tag` to update or create the tag for the current version
3. Run `make clean; autoconf; ./configure; make opam_file_update`
4. Commit and push _opam-repository/package/necrolib/necrolib.VERSION/opam_
5. Test deployment using the _Installation and test with OPAM_ script.

## New version

To create a new version, simply type `make new_version`, the current version will be
display, and will be asked for the new version number
